package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.domain.modelo.dao.AlumnoDAO;

import ar.edu.asap.practica0.modelo.PiedraPapelTieraFactory;



@Controller
public class IndexController {

	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}
	
	@RequestMapping("/")
	public String goPresentacion() {
		return "Presentacion";
	}
	@RequestMapping("/listado")
	public String goListado(Model model){
		AlumnoDAO aluDao;
		try {
			aluDao = new AlumnoDAO();
			List<com.domain.modelo.Model> alumnos = aluDao.leer(null); 
			
			model.addAttribute("titulo", "Listado de alumnos");
			model.addAttribute("profesor", "Gabriel Casas");
			model.addAttribute("alumnos", alumnos);

		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}
		
		return "Listado";
		
	}

	@RequestMapping("/juego")
	public String goJuego(Model model) {
		List<PiedraPapelTieraFactory> opciones = new ArrayList<PiedraPapelTieraFactory>();
		//llego el array a trav�s del factory
		for (int i =1;i<6;i++) {
			opciones.add(PiedraPapelTieraFactory.getInstance(i));
		}
		
		model.addAttribute("opciones", opciones);
	
		return "PiedraPapelTijera";
	}

	@RequestMapping("/resolverJuego")
	public String goResolverJuego(@RequestParam(required = false) Integer selOpcion, Model model) {
		
		System.out.println("******************** paso por /resolverJuego ********************:" + selOpcion );
		//seleccion de la computadora
		PiedraPapelTieraFactory computadora = PiedraPapelTieraFactory.getInstance((int)(Math.random()*100%5+1));
		//este es el parametro que viene desde el JSP
		PiedraPapelTieraFactory jugador = PiedraPapelTieraFactory.getInstance(selOpcion.intValue());
		
		jugador.comparar(computadora);
		
		model.addAttribute("jugador", jugador);
		model.addAttribute("computadora", computadora);
		model.addAttribute("resultado", jugador.getDescripcionREsultado());
		
		
		
		
		return "MostrarResultado";
	}
	
	
}
